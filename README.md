# dirtrim.pl

## What is it?

This is a little Perl 5 script I wrote ages ago to automatically keep the output directory of my `motion` camera setup within a sane size and not expand indefinitely until it filled up my hard drive.

You could use it to keep any given directory within a given size target. It deletes (or moves) the oldest files first in order to hit its target.

You can run it as part of a cron job or as a post-processing job linked with whatever task it is you've got running that creates all these files.

## Installation

`dirtrim.pl` is a Perl 5 script with no non-CORE module dependencies; you can just copy it wherever you want.

## Usage

First let me set up an example directory.

```shell
james@yang(): /tmp/dirtrim-example
$ cp -a ~/tmp/terrible-temp-catphone-backup/DCIM/OpenCamera/* .

james@yang(): /tmp/dirtrim-example
$ du -sh .
2.1G	.

james@yang(): /tmp/dirtrim-example
$ ~/source/bitbucket/dirtrim/dirtrim.pl . --size 100M --delete

james@yang(): /tmp/dirtrim-example
$ du -sh .
100M	.
```

As you can see, it works without any output by default - because you're only going to do `>/dev/null`in your cronjob to avoid spamming `root`'s inbox anyway.

But supposing we started with this directory, we'd first want confidence that the tool is going to do its job - so to start with, use the `--verbose` option with a `--size` argument and don't ask the tool to `--delete` or `--move` anything (dry-run mode):

```shell
james@yang(): /tmp/dirtrim-example
$ ~/source/bitbucket/dirtrim/dirtrim.pl . --size 80M --verbose
Keeping 47 files:-
Thu Nov 11 14:32:40 2021 2m ./IMG_20211111_143238_DRO.jpg
Thu Nov 11 14:32:32 2021 1m ./IMG_20211111_143229_DRO.jpg
Thu Nov 11 10:27:12 2021 2m ./IMG_20211111_102710_DRO.jpg
Tue Nov  9 17:29:16 2021 585k ./IMG_20211109_172915_DRO.jpg
Sat Nov  6 12:42:33 2021 1m ./IMG_20211106_124232_DRO.jpg
Sat Nov  6 12:42:31 2021 1m ./IMG_20211106_124229_DRO.jpg
Sat Nov  6 12:42:09 2021 1m ./IMG_20211106_124207_DRO.jpg
Fri Oct 29 20:58:07 2021 1m ./IMG_20211029_205804_DRO.jpg
Mon Oct 25 16:16:04 2021 2m ./IMG_20211025_161602_DRO.jpg
Fri Oct 22 19:58:24 2021 1m ./IMG_20211022_195822_DRO.jpg
Mon Oct 18 18:03:24 2021 1m ./IMG_20211018_180322_DRO.jpg
Mon Oct 18 14:22:53 2021 867k ./IMG_20211018_142252_DRO.jpg
Sat Oct 16 19:20:11 2021 1m ./IMG_20211016_192009_DRO.jpg
Sat Oct 16 18:04:14 2021 2m ./IMG_20211016_180412_DRO.jpg
Thu Oct  7 17:31:28 2021 2m ./IMG_20211007_173125_DRO.jpg
Sun Oct  3 16:42:01 2021 2m ./IMG_20211003_164158_DRO.jpg
Sun Oct  3 15:33:55 2021 1m ./IMG_20211003_153353_DRO.jpg
Sun Sep 26 16:32:35 2021 1m ./IMG_20210926_163233_DRO.jpg
Sun Sep 26 16:32:29 2021 2m ./IMG_20210926_163227_DRO.jpg
Sat Sep 25 01:50:27 2021 2m ./IMG_20210925_015025_DRO.jpg
Sat Sep 25 01:50:20 2021 2m ./IMG_20210925_015018_DRO.jpg
Sat Sep 25 01:50:14 2021 2m ./IMG_20210925_015012_DRO.jpg
Sat Sep 25 01:50:10 2021 2m ./IMG_20210925_015007_DRO.jpg
Tue Sep 21 09:07:06 2021 1m ./IMG_20210921_090704_DRO.jpg
Tue Sep 14 17:21:53 2021 682k ./IMG_20210914_172152_DRO.jpg
Wed Sep  8 17:02:46 2021 2m ./IMG_20210908_170243_DRO.jpg
Wed Sep  8 13:39:25 2021 840k ./IMG_20210908_133924_DRO.jpg
Tue Sep  7 09:08:08 2021 1m ./IMG_20210907_090805_DRO.jpg
Tue Aug 31 09:15:20 2021 1m ./IMG_20210831_091518_DRO.jpg
Sun Aug 29 11:02:32 2021 617k ./IMG_20210829_110230_DRO.jpg
Wed Aug 25 13:15:04 2021 1m ./IMG_20210825_131502_DRO.jpg
Wed Aug 25 13:14:59 2021 1m ./IMG_20210825_131457_DRO.jpg
Tue Aug 24 15:22:15 2021 2m ./IMG_20210824_152212_DRO.jpg
Tue Aug 24 12:45:23 2021 1m ./IMG_20210824_124521_DRO.jpg
Sat Aug 21 20:39:05 2021 2m ./IMG_20210821_203902_DRO.jpg
Mon Aug 16 13:58:56 2021 2m ./IMG_20210816_135853_DRO.jpg
Mon Aug 16 13:07:17 2021 561k ./IMG_20210816_130716_DRO.jpg
Fri Aug 13 11:39:16 2021 1m ./IMG_20210813_113914_DRO.jpg
Wed Aug 11 18:42:37 2021 1m ./IMG_20210811_184234_DRO.jpg
Mon Aug  9 07:57:40 2021 2m ./IMG_20210809_075737_DRO.jpg
Sat Aug  7 18:25:38 2021 1m ./IMG_20210807_182536_DRO.jpg
Fri Aug  6 17:33:43 2021 2m ./IMG_20210806_173340_DRO.jpg
Thu Aug  5 13:02:28 2021 677k ./IMG_20210805_130227_DRO.jpg
Tue Aug  3 09:25:48 2021 1m ./IMG_20210803_092545_DRO.jpg
Tue Jul 27 09:21:02 2021 1m ./IMG_20210727_092100_DRO.jpg
Tue Jul 27 09:19:57 2021 457k ./IMG_20210727_091957_DRO.jpg
Tue Jul 27 09:19:51 2021 450k ./IMG_20210727_091950_DRO.jpg

Would Cull 17 files:-
Tue Jul 27 09:19:49 2021 455k ./IMG_20210727_091948_DRO.jpg
Tue Jul 27 09:18:45 2021 402k ./IMG_20210727_091845_DRO.jpg
Mon Jul 19 17:28:48 2021 447k ./IMG_20210719_172847_DRO.jpg
Sun Jul 18 14:41:17 2021 663k ./IMG_20210718_144116_DRO.jpg
Fri Jul 16 22:38:49 2021 542k ./IMG_20210716_223848_DRO.jpg
Thu Jul  1 13:51:10 2021 496k ./IMG_20210701_135110_DRO.jpg
Thu Jul  1 13:50:51 2021 493k ./IMG_20210701_135050_DRO.jpg
Fri Jun 25 12:08:04 2021 480k ./IMG_20210625_120803_DRO.jpg
Tue Jun 22 12:39:51 2021 2m ./IMG_20210622_123949_DRO.jpg
Tue Jun 22 09:12:42 2021 1m ./IMG_20210622_091239_DRO.jpg
Mon Jun 21 19:36:36 2021 1m ./IMG_20210621_193634_DRO.jpg
Thu Jun 17 12:36:52 2021 2m ./IMG_20210617_123650_DRO.jpg
Fri Jun 11 08:15:26 2021 1m ./IMG_20210611_081524_DRO.jpg
Fri Jun 11 08:08:14 2021 2m ./IMG_20210611_080812_DRO.jpg
Mon Jun  7 21:59:34 2021 2m ./IMG_20210607_215932_DRO.jpg
Fri Jun  4 14:11:01 2021 441k ./IMG_20210604_141100_DRO.jpg
Fri Jun  4 12:13:35 2021 714k ./IMG_20210604_121334_DRO.jpg
```

Note that it's sorting the files based on their `mtime` - the default, which is usually what you want. Most UNIX filesystems also track `atime` (last access) and `ctime` (last inode change time e.g. metadata). You can choose to sort with these times instead using `--atime` and `--ctime` respectively. It's also possible that your filenames include a timestamp that lexically sorts nicely, so you can choose to use `--name` to sort the files by their name.

Only running the tool with the `--delete` or `--move <dirname>` options will perform any action. Delete is self-explanatory, and `--move` is recommended as an option if you either have some other way of disposing of old files in mind (archiving them with another script) or want them moved to a different partition that has more space available.

Running the above example for real:
```shell
james@yang(): /tmp/dirtrim-example
$ du -sh .
100M	.

james@yang(): /tmp/dirtrim-example
$ ~/source/bitbucket/dirtrim/dirtrim.pl . --size 80M --move /tmp/archive-me/ --verbose
Keeping 47 files:-
Thu Nov 11 14:32:40 2021 2m ./IMG_20211111_143238_DRO.jpg
Thu Nov 11 14:32:32 2021 1m ./IMG_20211111_143229_DRO.jpg
Thu Nov 11 10:27:12 2021 2m ./IMG_20211111_102710_DRO.jpg
Tue Nov  9 17:29:16 2021 585k ./IMG_20211109_172915_DRO.jpg
Sat Nov  6 12:42:33 2021 1m ./IMG_20211106_124232_DRO.jpg
Sat Nov  6 12:42:31 2021 1m ./IMG_20211106_124229_DRO.jpg
Sat Nov  6 12:42:09 2021 1m ./IMG_20211106_124207_DRO.jpg
Fri Oct 29 20:58:07 2021 1m ./IMG_20211029_205804_DRO.jpg
Mon Oct 25 16:16:04 2021 2m ./IMG_20211025_161602_DRO.jpg
Fri Oct 22 19:58:24 2021 1m ./IMG_20211022_195822_DRO.jpg
Mon Oct 18 18:03:24 2021 1m ./IMG_20211018_180322_DRO.jpg
Mon Oct 18 14:22:53 2021 867k ./IMG_20211018_142252_DRO.jpg
Sat Oct 16 19:20:11 2021 1m ./IMG_20211016_192009_DRO.jpg
Sat Oct 16 18:04:14 2021 2m ./IMG_20211016_180412_DRO.jpg
Thu Oct  7 17:31:28 2021 2m ./IMG_20211007_173125_DRO.jpg
Sun Oct  3 16:42:01 2021 2m ./IMG_20211003_164158_DRO.jpg
Sun Oct  3 15:33:55 2021 1m ./IMG_20211003_153353_DRO.jpg
Sun Sep 26 16:32:35 2021 1m ./IMG_20210926_163233_DRO.jpg
Sun Sep 26 16:32:29 2021 2m ./IMG_20210926_163227_DRO.jpg
Sat Sep 25 01:50:27 2021 2m ./IMG_20210925_015025_DRO.jpg
Sat Sep 25 01:50:20 2021 2m ./IMG_20210925_015018_DRO.jpg
Sat Sep 25 01:50:14 2021 2m ./IMG_20210925_015012_DRO.jpg
Sat Sep 25 01:50:10 2021 2m ./IMG_20210925_015007_DRO.jpg
Tue Sep 21 09:07:06 2021 1m ./IMG_20210921_090704_DRO.jpg
Tue Sep 14 17:21:53 2021 682k ./IMG_20210914_172152_DRO.jpg
Wed Sep  8 17:02:46 2021 2m ./IMG_20210908_170243_DRO.jpg
Wed Sep  8 13:39:25 2021 840k ./IMG_20210908_133924_DRO.jpg
Tue Sep  7 09:08:08 2021 1m ./IMG_20210907_090805_DRO.jpg
Tue Aug 31 09:15:20 2021 1m ./IMG_20210831_091518_DRO.jpg
Sun Aug 29 11:02:32 2021 617k ./IMG_20210829_110230_DRO.jpg
Wed Aug 25 13:15:04 2021 1m ./IMG_20210825_131502_DRO.jpg
Wed Aug 25 13:14:59 2021 1m ./IMG_20210825_131457_DRO.jpg
Tue Aug 24 15:22:15 2021 2m ./IMG_20210824_152212_DRO.jpg
Tue Aug 24 12:45:23 2021 1m ./IMG_20210824_124521_DRO.jpg
Sat Aug 21 20:39:05 2021 2m ./IMG_20210821_203902_DRO.jpg
Mon Aug 16 13:58:56 2021 2m ./IMG_20210816_135853_DRO.jpg
Mon Aug 16 13:07:17 2021 561k ./IMG_20210816_130716_DRO.jpg
Fri Aug 13 11:39:16 2021 1m ./IMG_20210813_113914_DRO.jpg
Wed Aug 11 18:42:37 2021 1m ./IMG_20210811_184234_DRO.jpg
Mon Aug  9 07:57:40 2021 2m ./IMG_20210809_075737_DRO.jpg
Sat Aug  7 18:25:38 2021 1m ./IMG_20210807_182536_DRO.jpg
Fri Aug  6 17:33:43 2021 2m ./IMG_20210806_173340_DRO.jpg
Thu Aug  5 13:02:28 2021 677k ./IMG_20210805_130227_DRO.jpg
Tue Aug  3 09:25:48 2021 1m ./IMG_20210803_092545_DRO.jpg
Tue Jul 27 09:21:02 2021 1m ./IMG_20210727_092100_DRO.jpg
Tue Jul 27 09:19:57 2021 457k ./IMG_20210727_091957_DRO.jpg
Tue Jul 27 09:19:51 2021 450k ./IMG_20210727_091950_DRO.jpg

Moving 17 files:-
Tue Jul 27 09:19:49 2021 455k ./IMG_20210727_091948_DRO.jpg
Tue Jul 27 09:18:45 2021 402k ./IMG_20210727_091845_DRO.jpg
Mon Jul 19 17:28:48 2021 447k ./IMG_20210719_172847_DRO.jpg
Sun Jul 18 14:41:17 2021 663k ./IMG_20210718_144116_DRO.jpg
Fri Jul 16 22:38:49 2021 542k ./IMG_20210716_223848_DRO.jpg
Thu Jul  1 13:51:10 2021 496k ./IMG_20210701_135110_DRO.jpg
Thu Jul  1 13:50:51 2021 493k ./IMG_20210701_135050_DRO.jpg
Fri Jun 25 12:08:04 2021 480k ./IMG_20210625_120803_DRO.jpg
Tue Jun 22 12:39:51 2021 2m ./IMG_20210622_123949_DRO.jpg
Tue Jun 22 09:12:42 2021 1m ./IMG_20210622_091239_DRO.jpg
Mon Jun 21 19:36:36 2021 1m ./IMG_20210621_193634_DRO.jpg
Thu Jun 17 12:36:52 2021 2m ./IMG_20210617_123650_DRO.jpg
Fri Jun 11 08:15:26 2021 1m ./IMG_20210611_081524_DRO.jpg
Fri Jun 11 08:08:14 2021 2m ./IMG_20210611_080812_DRO.jpg
Mon Jun  7 21:59:34 2021 2m ./IMG_20210607_215932_DRO.jpg
Fri Jun  4 14:11:01 2021 441k ./IMG_20210604_141100_DRO.jpg
Fri Jun  4 12:13:35 2021 714k ./IMG_20210604_121334_DRO.jpg

james@yang(): /tmp/dirtrim-example
$ du -sh .
80M	.
```

# Licence

This program can be modified and distributed under the terms of the Perl Artistic Licence 2.0 - see the LICENCE file or https://www.perlfoundation.org/artistic-license-20.html
