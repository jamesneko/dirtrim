#!/usr/bin/perl

# Script to delete the oldest files from a directory to satisfy a total size requirement.
# Copyright (c) 2013 James Clark
# This file can be modified and distributed under the terms of the Perl Artistic Licence 2.0 - see the LICENCE file or https://www.perlfoundation.org/artistic-license-20.html

use warnings;
use strict;
use File::Find;
use File::Copy;
use Getopt::Long;

my %OPTS;
GetOptions(\%OPTS, 'help!', 'delete!', 'move=s', 'size=s', 'atime!', 'mtime!', 'ctime!', 'name!', 'verbose!');

my @FILES;  # list of hashrefs, {name, size, atime, mtime, ctime}. Populated by find_callback.
my $TIME_ATTR = "mtime";  # What to sort by.


sub help
{
  print STDERR <<EOF;
Usage: $0 --size <size>{K,M,G} dirname/ --delete
       $0 --size <size>{K,M,G} dirname/ --move destdir/
       
       Omitting --delete and --move does a dry-run.

       --atime : Sort by last access time
       --mtime : Sort by last modified time (default)
       --ctime : Sort by last inode change time
       --name  : Sort by file basename
       
       --verbose : Show the list of files with timestamps

Sorts files contained in the given directory according to age, and deletes
the oldest files until the total size of the directory is under the given
size limit.
EOF
}


sub main
{
  my $dir = shift @ARGV;
  die "Need to supply a directory name!" unless (defined $dir && -d $dir);
  die "Need to supply a --size!" unless (defined $OPTS{size});
  my $size = parse_size($OPTS{size}) or die "Size parameter not recognised!";
  die "--move option needs a directory to move to!" if ($OPTS{move} && ! -d $OPTS{move});
    
  # Adjust options based on command line.
  $TIME_ATTR = "atime" if ($OPTS{atime});
  $TIME_ATTR = "mtime" if ($OPTS{mtime});
  $TIME_ATTR = "ctime" if ($OPTS{ctime});
  $TIME_ATTR = "basename" if ($OPTS{name});
  
  # Collate file info.
  find(\&find_callback, $dir);
  
  # Sort by mtime (or whatever)
  my @sorted_files = reverse sort by_time_attr @FILES;  # highest mtime first (oldest last)
  
  # Make a cut if you go above the limit.
  my ($keep_files, $cull_files) = split_files_after_limit($size, @sorted_files);
  
  
  print "Keeping ", scalar @$keep_files, " files:-\n" if ($OPTS{verbose});
  print_files(@$keep_files) if ($OPTS{verbose});
  
  if ($OPTS{move}) {
    print "\nMoving ", scalar @$cull_files, " files:-\n" if ($OPTS{verbose});
    print_files(@$cull_files) if ($OPTS{verbose});
    move_files(@$cull_files);
    
  } elsif ($OPTS{delete}) {
    print "\nDeleting ", scalar @$cull_files, " files:-\n" if ($OPTS{verbose});
    print_files(@$cull_files) if ($OPTS{verbose});
    delete_files(@$cull_files);
  
  } else {
    print "\nWould Cull ", scalar @$cull_files, " files:-\n" if ($OPTS{verbose});
    print_files(@$cull_files) if ($OPTS{verbose});

  }

}


sub find_callback
{
  # Don't match dotfiles.
  return if ($_ =~ /^\./);
  # We only care about files, not dirs.
  return unless (-f $_);
  # Collect stats.
  my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,$atime,$mtime,$ctime,$blksize,$blocks) = stat($_);
  push @FILES, { pathname => $File::Find::name,
                 basename => $_,
                 size => $size,
                 atime => $atime,
                 mtime => $mtime,
                 ctime => $ctime,
               };
}


sub by_time_attr
{
  if ($TIME_ATTR eq 'basename') {
    $a->{$TIME_ATTR} cmp $b->{$TIME_ATTR};
  } else {
    $a->{$TIME_ATTR} <=> $b->{$TIME_ATTR};
  }
}


sub print_files
{
  foreach my $file (@_) {
    print $TIME_ATTR ne 'basename' ? scalar localtime($file->{$TIME_ATTR}) : "", 
          " ", express_size($file->{size}), 
          " $file->{pathname}\n";
  }
}


sub move_files
{
  foreach my $file (@_) {
    move($file->{pathname}, $OPTS{move});
  }
}


sub delete_files
{
  foreach my $file (@_) {
    unlink $file->{pathname};
  }
}


sub split_files_after_limit
{
  my ($limit, @files) = @_;
  my (@keep, @cull);
  my $running_total = 0;
  
  foreach my $file (@files) {
    $running_total += $file->{size};
    
    # If this file is the one that puts us over the limit, it and everything afterwards
    # goes in 'cull'.
    if ($running_total > $limit) {
      push @cull, $file;
    } else {
      push @keep, $file;
    }
  }
  
  return (\@keep, \@cull);
}


sub parse_size($)
{
  if ($_[0] =~ /^
                (?<size>       \d+)
                (?<quantifier> [kKmMgG]?)
                $
               /x) {
    # Because I'm about to do more matching, I need to stash the named captures
    # into real variables.
    my $size = $+{size};
    my $quantifier = $+{quantifier};
    $size *= 1024 if ($quantifier =~ /[kKmMgG]/);
    $size *= 1024 if ($quantifier =~ /[mMgG]/);
    $size *= 1024 if ($quantifier =~ /[gG]/);
    return $size;
  }
}


sub express_size($)
{
  my ($bytes) = @_;
  if ($bytes > 1024**3) {
    return int($bytes / 1024**3) . "g";
  } elsif ($bytes > 1024**2) {
    return int($bytes / 1024**2) . "m";
  } elsif ($bytes > 1024**1) {
    return int($bytes / 1024**1) . "k";
  } else {
    return $bytes;
  }
}



if ($OPTS{help}) {
  help();
} else {
  main();
}
